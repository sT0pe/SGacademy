<?php
$task = 1;
if(isset($_GET['task']) && $_GET['task'] != ''){
    $task = $_GET['task'];
}
////////////////////////////////////////////////////////////////////////////////////////////
// Зчитування даних з файлу в асоціативний масив
////////////////////////////////////////////////////////////////////////////////////////////
$array = file('airlines.txt',FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$array = array_chunk($array, 8);
$data = array();
foreach ($array as $key => $arr){
    $data[$key]['name']    = substr($arr[0], 2, -1);
    $data[$key]['company'] = substr($arr[1], 2, -1);
    $data[$key]['pilot']   = substr($arr[2], 2, -1);
    $data[$key]['from']    = substr($arr[3], 2, -1);
    $data[$key]['to']      = substr($arr[4], 2, -1);
    $data[$key]['price']   = substr($arr[5], 2, -1);
    $data[$key]['time']    = substr($arr[6], 2, -1);
    $data[$key]['plane']   = substr($arr[7], 2, -1);
}


////////////////////////////////////////////////////////////////////////////////////////////
// Добавлення нових записів у файл
////////////////////////////////////////////////////////////////////////////////////////////
$new_data = '';
if(isset($_POST['name']) && $_POST['name'] != ''){
    $new_data = PHP_EOL . PHP_EOL . '- ' . $_POST['name'] . ';';
}
if(isset($_POST['company']) && $_POST['company'] != ''){
    $new_data .= PHP_EOL . '- ' . $_POST['company'] . ';';
}
if(isset($_POST['pilot']) && $_POST['pilot'] != ''){
    $new_data .= PHP_EOL . '- ' . $_POST['pilot'] . ';';
}
if(isset($_POST['from']) && $_POST['from'] != '') {
    $new_data .= PHP_EOL . '- ' . $_POST['from'] . ';';
}
if(isset($_POST['to']) && $_POST['to'] != '') {
    $new_data .= PHP_EOL . '- ' . $_POST['to'] . ';';
}
if(isset($_POST['price']) && $_POST['price'] != '') {
    $new_data .= PHP_EOL . '- ' . $_POST['price'] . ';';
}
if(isset($_POST['time']) && $_POST['time'] != '') {
    $new_data .= PHP_EOL . '- ' . $_POST['time'] . ';';
}
if(isset($_POST['plane']) && $_POST['plane'] != '') {
    $new_data .= PHP_EOL . '- ' . $_POST['plane'];
}

    if ($new_data != '') {
        $fp = fopen('airlines.txt', 'a');
        fwrite($fp, $new_data);
        fclose($fp);
        header('Location: .');
        exit();
    }


////////////////////////////////////////////////////////////////////////////////////////////
// Вивід рейсів в порядку зростання вартості квитка
////////////////////////////////////////////////////////////////////////////////////////////
function sort_function ($first, $second){
    if ($first['price'] > $second['price']) { return -1; }
    if ($first['price'] < $second['price']) { return 1; }
}
$data_sort = $data;
usort($data_sort, 'sort_function');


////////////////////////////////////////////////////////////////////////////////////////////
// Кількість рейсів, що відлітають до Львова
////////////////////////////////////////////////////////////////////////////////////////////
$quantity = 0;
$data_to = array();
foreach ($data as $array) {
    if ($array['to'] == 'Львів'){
        $quantity++;
        $data_to[] = $array;
    }
}


////////////////////////////////////////////////////////////////////////////////////////////
// Пілоти з певним набором символів у прізвищі
////////////////////////////////////////////////////////////////////////////////////////////
$result = '';
if(isset($_POST['symbols']) && $_POST['symbols'] != ''){
    $needle = $_POST['symbols'];
    $data_pilot = array();
    foreach ($data as $array) {
       $haystack = explode(" ",$array['pilot']);
       if(mb_stripos($haystack[0], $needle, 0, 'UTF-8') !== false){
           $data_pilot[] = $array;
       }
    }
    if(empty($data_pilot)) {
        $result = 'Рейси відсутні!';
    }
}

include 'home.php';
