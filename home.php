<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SGAcademy</title>
    <link href="stylesheet.css" rel="stylesheet">
</head>
<body>
<nav id="horizontal">
    <ul class="menu">
        <li <?php if($task == 1) echo 'style="background-color: lavender;"'; ?>><a href="?task=1">Завдання 1</a></li>
        <li <?php if($task == 2) echo 'style="background-color: lavender;"'; ?>><a href="?task=2">Завдання 2</a></li>
        <li <?php if($task == 3) echo 'style="background-color: lavender;"'; ?>><a href="?task=3">Завдання 3</a></li>
        <li <?php if($task == 4) echo 'style="background-color: lavender;"'; ?>><a href="?task=4">Завдання 4</a></li>
        <li <?php if($task == 5) echo 'style="background-color: lavender;"'; ?>><a href="?task=5">Завдання 5</a></li>
    </ul>
</nav>
<?php
switch ($task) {
    case '1':
        include "answers/1.php";
        break;
    case '2':
        include "answers/2.php";
        break;
    case '3':
        include "answers/3.php";
        break;
    case '4':
        include "answers/4.php";
        break;
    case '5':
        include "answers/5.php";
        break;
    default:
        include 'answers/1.php';
} ?>
</body>
</html>