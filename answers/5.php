<h2>Форма для пошуку рейсів за кермом яких будуть пілоти із певним набором символів у прізвищі</h2>
<form action="" method="post">
    <label for="symbols">Введіть дані:</label><input id="symbols" name="symbols" value="<?php echo $needle;?>"/>
    <input type="submit" value="Знайти"/>
</form>
<?php
if(!empty($data_pilot)){
    foreach ($data_pilot as $key => $array){
        echo '<h2>Рейс ' . ++$key . '</h2>'; ?>
        <table>
            <tr>
                <td>Назва рейсу</td>
                <td>Перевізник</td>
                <td>Пілот</td>
                <td>Звідки</td>
                <td>Куда</td>
                <td>Вартість, UAH</td>
                <td>Час вильоту</td>
                <td>Марка літака</td>
            </tr>
            <tr>
                <?php foreach ($array as $arr){
                    echo '<td>' . $arr . '</td>';
                } ?>
            </tr>
        </table>
        <?php
    }
} else {
    echo $result;
}
?>