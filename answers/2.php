<!-- Форма для добалення нового запису у файл -->
<form class="form" action="" method="post">
    <ul>
        <li>
            <h1>Добавити новий рейс</h1>
            <span class="required_notification">* - обов'язкові поля</span>
        </li>
        <li>
            <label for="name">Назва рейсу<span class="required">*</span> </label>
            <input type="text" id="name" name="name" placeholder="Назва рейсу" required/>
        </li>
        <li>
            <label for="company">Перевізник<span class="required">*</span> </label>
            <input type="text" id="company" name="company" placeholder="Назва компанії" required/>
        </li>
        <li>
            <label for="pilot">Пілот<span class="required">*</span> </label>
            <input type="text" id="pilot" name="pilot" placeholder="Прізвище Ім'я" required pattern="[a-zA-Zа-яА-ЯіїІЇєЄ]+ [a-zA-Zа-яА-ЯіІїЇєЄ]+"/>
        </li>
        <li>
            <label for="from">Звідки<span class="required">*</span> </label>
            <input type="text" id="from" name="from" placeholder="Місто" required pattern="[a-zA-Zа-яА-ЯіїІЇєЄ\- ]+"/>
        </li>
        <li>
            <label for="to">Куда<span class="required">*</span> </label>
            <input type="text" id="to" name="to" placeholder="Місто" required pattern="[a-zA-Zа-яА-ЯіїІЇєЄ\- ]+"/>
        </li>
        <li>
            <label for="price">Вартість<span class="required">*</span> </label>
            <input type="text" id="price" name="price" placeholder="формат 0.00" required pattern="\d+\.\d{2}"/>
        </li>
        <li>
            <label for="time">Час вильоту<span class="required">*</span> </label>
            <input type="text" id="time" name="time" placeholder="формат 00:00" required pattern="([01]?[0-9]|2[0-3]):[0-5][0-9]"/>
        </li>
        <li>
            <label for="plane">Марка літака<span class="required">*</span> </label>
            <input type="text" id="plane" name="plane" placeholder="Марка" required/>
        </li>
        <li><button class="submit" type="submit">Добавити</button>
    </ul>
</form>